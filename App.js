/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import {
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  View,
  TextInput,
  Button,
  Alert,
  BackHandler
} from 'react-native';

import RNExitApp from 'react-native-exit-app';

import RNLockTask from 'react-native-lock-task';

RNLockTask.startLockTask();
RNLockTask.startLockTaskWith(["com.testrn"]);

export default function App() {
  const [value, onChange] = React.useState('');

  const logout = () => {
    if (value === '1234') {
      RNLockTask.stopLockTask();
      RNLockTask.clearDeviceOwnerApp();
      // RNExitApp.exitApp()
    } else {
      Alert.alert('Ошибка!', 'Введен неверный код')
    }
  }

  return (
    <SafeAreaView>
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        >
        <View style={{
          flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
        }}>
          <Image source={require('./assets/logo.png')} style={{ width: 300, height: 300}}/>
          <View>
            <TextInput 
              value={value} 
              onChangeText={val => onChange(val)}
              keyboardType='numeric'
              maxLength={4}
              placeholder='Введите код'
              textAlign='center'
              style={{
                borderWidth: 1,
                padding: 6,
                marginTop: 12,
                marginBottom: 12,
                width: 250
              }} />

            <Button title='Разблокировать' onPress={logout} />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  
});
